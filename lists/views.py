from django.shortcuts import redirect, render
from lists.models import Question, Answer
from datetime import datetime

def home_page(request):
    day = 0
    month = 0
    year = 0
    if request.method == 'POST' and request.POST.get('send_add', '') == 'send_Add':
        return redirect('/add_question')

    if request.method == 'POST' and request.POST.get('id_show', '') == 'id_show':
            date = datetime.today()
            if request.POST['day'] != '-':
                d = int(request.POST['day'])
                day = date.day - d
            if request.POST['month'] != '-':
                m = int(request.POST['month'])
                month = date.month - m
            if request.POST['year'] != '-':
                y = int(request.POST['year'])
                year = date.year - y
            questions = Question.objects.all().order_by('-id')
            return render(request, 'home.html', {'questions': questions,'day':day,'month':month,'year':year})


    if request.method == 'POST':
        return redirect('/')
    questions = Question.objects.all().order_by('-id')
    return render(request, 'home.html', {'questions': questions,'day':day,'month':month,'year':year})

def add_page(request):

    if request.method == 'POST' and request.POST.get('send_add', '') == 'send_Add':
        if request.POST['new_question'] != '':
            question_ = Question.objects.create(text=request.POST['new_question'],time=datetime.now())
            return redirect('/add_answer/%d' % int(question_.id), {'question_':question_,})

    return render(request, 'add.html')

def add_answer_page(request, question_id):
    question_ = Question.objects.get(id=question_id)
    answer_ = Answer.objects.filter(question=question_)
    if request.method == 'POST' and request.POST.get('send_answer', '') == 'send_answer':
        if request.POST['new_answer'] != '':
            Answer.objects.create(answer=request.POST['new_answer'],answer_count=0,question=question_)
            answer_ = Answer.objects.filter(question=question_)
            return redirect('/add_answer/%d' % int(question_id), {'question_':question_, 'answer_': answer_})

    return render(request, 'add_answer.html',{'question_':question_, 'answer_': answer_})

def view_question_page(request, question_id):
    question_ = Question.objects.get(id=question_id)
    answer_ = Answer.objects.filter(question=question_)
    if(request.method == 'POST' and request.POST.get(
      'send_choose', '') == 'send_choose'):
        id_answer = request.POST['id_answer']
        answer_ = Answer.objects.get(id=id_answer)
        add_answer = int(answer_.answer_count) + 1
        answer_.answer_count = add_answer
        question_.total_count = int(question_.total_count) + 1
        answer_.save()
        question_.save()
        return redirect('/result/%d' % int(question_id),{"question_":question_, "answer_":answer_})
    return render(request, 'view_question.html', {"question_":question_, "answer_":answer_})

def view_result_page(request, question_id):
    question_ = Question.objects.get(id=question_id)
    answer_ = Answer.objects.filter(question=question_)
    return render(request, 'result.html', {"question_":question_, "answer_":answer_})
