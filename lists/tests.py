from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
from datetime import datetime

from lists.models import Question, Answer
from lists.views import home_page, add_page, add_answer_page, view_question_page, view_result_page

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)


class AddPageTest(TestCase):

    def test_root_url_resolves_to_add_page_view(self):
        found = resolve('/add_question')
        self.assertEqual(found.func, add_page)

    def test_add_page_returns_correct_html(self):
        request = HttpRequest()
        response = add_page(request)
        expected_html = render_to_string('add.html')
        self.assertEqual(response.content.decode(), expected_html)

    def test_add_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['new_question'] = 'What is Django?'
        request.POST['send_add'] = 'send_Add'

        response = add_page(request)
        date = datetime.now()

        new_question = Question.objects.first()
        self.assertEqual(new_question.text, 'What is Django?')
        self.assertEqual(new_question.time, date)

class QuestionModelTest(TestCase):

    def test_saving_and_retrieving_questions(self):
        first_question = Question()
        first_question.text = 'What is the meaning of life?'
        first_question.save()

        second_question = Question()
        second_question.text = 'What can we do for the world?'
        second_question.save()

        saved_questions = Question.objects.all()
        self.assertEqual(saved_questions.count(), 2)

        first_saved_question = saved_questions[0]
        second_saved_question = saved_questions[1]
        self.assertEqual(first_saved_question.text, 'What is the meaning of life?')
        self.assertEqual(second_saved_question.text, 'What can we do for the world?')

class AnswerModelTest(TestCase):

    def test_saving_and_retrieving_answers(self):
        question = Question()
        question.text = 'What is the meaning of life?'
        question.save()

        saved_question = Question.objects.first()
        question_id = saved_question.id
        question_ = Question.objects.get(id=question_id)

        first_answer = Answer()
        first_answer.answer = 'love'
        first_answer.question = saved_question
        first_answer.save()

        second_answer = Answer()
        second_answer.answer = 'peaceful'
        second_answer.question = saved_question
        second_answer.save()

        saved_answers = Answer.objects.filter(question=saved_question)
        self.assertEqual(saved_answers.count(), 2)

        first_saved_answer = saved_answers[0]
        second_saved_answer = saved_answers[1]
        self.assertEqual(first_saved_answer.answer, 'love')
        self.assertEqual(second_saved_answer.answer, 'peaceful')

