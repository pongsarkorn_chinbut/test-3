from django.db import models
from datetime import datetime

class Question(models.Model):
    text = models.TextField(default='')
    time = models.DateTimeField(default=datetime.now, blank=True)
    total_count = models.IntegerField(default=0)

class Answer(models.Model):
    question = models.ForeignKey(Question, default=None)
    answer = models.TextField(default='')
    answer_count = models.IntegerField(default=0)
