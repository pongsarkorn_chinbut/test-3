from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'lists.views.home_page', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^add_question$', 'lists.views.add_page',name='add question'),
    url(r'^add_answer/(\d+)$', 'lists.views.add_answer_page',name='add answer'),
    url(r'^question/(\d+)$', 'lists.views.view_question_page',name='view'),
    url(r'^result/(\d+)$', 'lists.views.view_result_page',name='result'),
    # url(r'^admin/', include(admin.site.urls)),
)
