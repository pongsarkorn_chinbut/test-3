from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from datetime import datetime
import unittest

class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()


    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def add_question(self, question, answers):
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Question App', header_text)
        self.browser.find_element_by_id('add_question').click()
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Add Question', header_text)
        question_textbox = self.browser.find_element_by_id('id_new_question')
        self.assertEqual(
                question_textbox.get_attribute('placeholder'),
                'Add Question'
        )
        question_textbox.send_keys(question)
        self.browser.find_element_by_id('add_question').click()
        date = datetime.now()
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Add Answer of Question "'+question+'"', header_text)
        for answer in answers:
            answer_textbox = self.browser.find_element_by_id('id_new_answer')
            self.assertEqual(
                    answer_textbox.get_attribute('placeholder'),
                    'Add Answer'
            )
            answer_textbox.send_keys(answer)
            self.browser.find_element_by_id('add_answer').click()
        self.browser.find_element_by_id('id_done').click()
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Question App', header_text)
        return date

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Question App', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Question App', header_text)
        first_date = self.add_question('What is your favorite animal?', ['A dog', 'A cat', 'A bird'])

        #table = self.browser.find_element_by_id('id_question_list_table')
        #rows = table.find_elements_by_tag_name('tr')
        #self.assertIn('What is your favorite animal? 0 '+str(first_date.day)+" "+str(first_date.month)+" "+str(first_date.year)+" "+str(first_date.hour)+":"+str(first_date.minute)+" "+str(first_date.ToString("tt")), [row.text for row in rows])

        second_date = self.add_question('What is your favorite color?', ['Red', 'Blue', 'Green', 'Yellow', 'Pink'])

        #table = self.browser.find_element_by_id('id_question_list_table')
        #rows = table.find_elements_by_tag_name('tr')
        #self.assertIn('What is your favorite animal? 0 '+str(first_date.day)+" "+str(first_date.month)+" "+str(first_date.year)+" "+str(first_date.hour)+":"+str(first_date.minute)+" "+str(first_date.ampm()), [row.text for row in rows])


if __name__ == '__main__':
    unittest.main(warnings='ignore')
